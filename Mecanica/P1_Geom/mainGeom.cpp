#pragma once;
#include <iostream>  //per escriure a consola
#include <string>    //per poder passar el nom al cout
#include <fstream>   //per escriure a fitxer
#include <chrono>    //per comptar el temps d'execucio 
#include <vector>    //per crear vectors d'estructures tipus: Point, Lines, etc.
#include <math.h>

#include "Geometry.h"
#define PRINT_VEC3(_v) std::cout << _v.x << " " << _v.y << " " << _v.z << std::endl;
#define FPRINT_VEC3(_v) fileOut << _v.x << " " << _v.y << " " << _v.z << "\n";


//for lazy people....
#define tempsActual std::chrono::high_resolution_clock::now()  

void main()
{
	Triangle triangle(glm::vec3(-3, -4, 4), glm::vec3(2, 0, 5), glm::vec3(0, -5, 7));
	glm::vec3 v1(5.253506, -1.33217, 5.577349);
	std::cout << triangle.isInside(v1);

	//Esfera: 3.26015 3.76447 1.88223
	//Plano: 2.73212 3.15474 1.57737
	std::cout << "Hemos convertido los grados en radianes para hacer los calculos." << std::endl;
	/*Point point (glm::vec3(3, 0, 0));
	Point point1(glm::vec3(3, 4, 6));
	Point point2(glm::vec3(-1, 0, 4));

	Line pepe(point1, point2);

	float pino = pepe.distLine2Point(point);
	std::cout << pino;*/

	Line rayo(glm::vec3(0, 0, 0), glm::vec3(cos(glm::radians(34.0f)) * cos(glm::radians(30.0f)), sin(glm::radians(30.0f)), sin(glm::radians(34.0f))* cos(glm::radians(30.0f))));
	//Line rayo(glm::vec3(0, 0, 0), glm::vec3(cos(30) * cos(45), sin(45), sin(30)* cos(45)));
	//Line rayo(glm::vec3(1, 2, 1), glm::vec3(5, 5, 5));
	Sphere esfera(glm::vec3(3, 2, 2), 2);
	float Alpha_Esfera;
	for (float i = 0; i < 500; i = i + 0.01)
	{
		glm::vec3 puntoesfera = rayo.point.position + (rayo.direction * i);
		
		if (esfera.isInside(puntoesfera) == false)
		{
			Alpha_Esfera = i;
			std::cout << Alpha_Esfera;
			break;
		}
		else
		{

		}
	}
	glm::vec3 PuntoCorteEsfera;
	float Alpha_Esfera2 = Alpha_Esfera - 0.01;
	glm::vec3 potato = rayo.point.position + (rayo.direction * (Alpha_Esfera));
	glm::vec3 potato2 = rayo.point.position + (rayo.direction * (Alpha_Esfera2));
	esfera.intersecSegment(potato2, potato, PuntoCorteEsfera);
	std::cout << "Punto de corte con la esfera " << std::endl;
	PRINT_VEC3(PuntoCorteEsfera);



	Plane plano(glm::vec3(1, 2, 1), glm::vec3(-1, 1, 1));
	float Alpha_Plano;
	for (float i = 0; i < 10; i = i + 0.01)
	{	
		glm::vec3 puntoplano = rayo.point.position + (rayo.direction * i);
		glm::vec3 puntoplanosiguiente = rayo.point.position + (rayo.direction * (i-0.01f));
		if ((glm::dot(plano.normal, puntoplano) + plano.dconst)*(glm::dot(plano.normal, puntoplanosiguiente) + plano.dconst) < 0)//plano.isInside(puntoplano) == true)
		{
			Alpha_Plano = i;
			break;
		}
		else
		{

		}
	}
	glm::vec3 PuntoCortePlano;
	float Alpha_Plano2 = Alpha_Plano - 0.01;
    potato = rayo.point.position + (rayo.direction * (Alpha_Plano));
	potato2 = rayo.point.position + (rayo.direction * (Alpha_Plano2));
	plano.intersecSegment(potato2, potato, PuntoCortePlano);
	std::cout << "Punto de corte con el plano " << std::endl;
	PRINT_VEC3(PuntoCortePlano);


	Plane planotriangulo(glm::vec3(1, 2, 1), glm::vec3(-1, 1, 2), glm::vec3(0, -1, 0));//plano 4x-3y+5z-3 = 0
	Triangle triangulo(glm::vec3(1,2,1), glm::vec3(-1,1,2), glm::vec3(0,-1,0));
	float Alpha_PlanoTriangulo;

	for (float i = 0; i < 10; i = i + 0.01)
	{
		glm::vec3 puntotriangulo = rayo.point.position + rayo.direction * i;
		glm::vec3 puntoplanosiguiente = rayo.point.position + (rayo.direction * (i + 0.01f));
		if ((glm::dot(planotriangulo.normal, puntotriangulo) + planotriangulo.dconst)*(glm::dot(planotriangulo.normal, puntoplanosiguiente) + plano.dconst) < 0)
		{
			Alpha_PlanoTriangulo = i;
			i = 10;
		}
		else
		{

		}
	}

	glm::vec3 PuntoCortePlanoTriangulo;
	if (planotriangulo.intersecSegment(rayo.point.position + glm::vec3((Alpha_Plano - 0.01) * rayo.direction.x, (Alpha_Plano - 0.01)  * rayo.direction.y, (Alpha_Plano - 0.01) * rayo.direction.z), rayo.point.position + glm::vec3(Alpha_Plano * rayo.direction.x, Alpha_Plano * rayo.direction.y, Alpha_Plano * rayo.direction.z), PuntoCortePlanoTriangulo) == true)
	{
		if (triangulo.isInside(PuntoCortePlanoTriangulo) == true)
		{
			std::cout << "Punto de corte con el triangulo" << std::endl;
			PRINT_VEC3(PuntoCortePlanoTriangulo);
		}

		else
		{
			std::cout << "No hay interseccion con el triangulo" << std::endl;
		}
	}
	else
	{
		std::cout << "No hay interseccion con el triangulo" << std::endl;
	}

	system("pause");

	/*
	//Fitxer de resultats
	std::string filename = "results.txt";

	std::ofstream fileOut(filename, std::ios::out);
	if (!fileOut)
	{
		std::cerr << "Cannot open " << filename << std::endl;
		exit(1);
	}

	//Geometry
	Point p, q, r;
	std::chrono::high_resolution_clock::time_point tini, tfin;
	tini = tempsActual; //the same as  std::chrono::high_resolution_clock::now();

	/// Test Point methods
	float alfa = 0.5;
	float x = 1.0f; float y = 0.5f; float z = 1.5f;
	glm::vec3 pos(0, 0.1,-2);
	p.setPosition(pos);
	q.setPosition(x, y, z);
	//print values
	std::cout << "Position point p" << std::endl;
	//	std::cout << p.position.x << "  " << p.position.y << "  " << p.position.z << std::endl;
	PRINT_VEC3(p.position);
	FPRINT_VEC3(p.position); //write into a file
	std::cout << "Position point q" << std::endl;
//	std::cout << q.position.x << "  " << q.position.y << "  " << q.position.z << std::endl;
	PRINT_VEC3(q.position);
	FPRINT_VEC3(q.position);
	//compute distance (length of the vector q-p)
	float dist = glm::length(p.position - q.position);
	// a consola
	std::cout << "distancia de p a q = " << dist << std::endl;
	std::cout << "distancia entre els punts p i q = " << p.distPoint2Point(q) << std::endl;
	// a fitxer
	fileOut << "distancia de p a q = " << dist << "\n";
	fileOut << "distancia entre els punts p i q = " << p.distPoint2Point(q) << "\n";


	//move the point q until it is far from p (>5 units)
	std::cout << std::endl;
	std::cout << "separem els punts  " << std::endl;
	for (int i = 0; i < 11; i++){
		q.setPosition(x+i, y, z);
		std::cout << " iter = " << i << " present point = " << q.position.x << "  " << q.position.y << "  " << q.position.z << std::endl;
		std::cout << "dist to p =" << p.distPoint2Point(q.position) << std::endl;
		if (p.distPoint2Point(q.position) > 5){
			std::cout << "hem passat de 5 " << std::endl;
			break;
		}
	}


	
	r = p.pointInSegment(q, alfa); //punt mig del segment pq, ja que alfa =0.5
	std::cout << std::endl;
	std::cout << "Segment pq point at value alfa = " << alfa << std::endl;
	std::cout << r.position.x << "  " << r.position.y << "  " << r.position.z << std::endl;
	std::cout << "verify dist to mid point: " << p.distPoint2Point(r) << std::endl; 
	std::cout << "verify mid distance between the points: " << 0.5f*p.distPoint2Point(q) << std::endl;

	tfin = tempsActual; //std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(tfin - tini).count();
	std::cout << "milliseconds = " << duration << std::endl;

	system("pause");

	/// Test Line methods
	Line line;
	line.point = glm::vec3(0); //origen of coordinates
	line.setDirection(glm::vec3(1, 0, 0)); //inicialitzacio com a vec3
	q.setPosition(0.0f, 2.0f, 0.0f); //inicialitzacio com a floats
	std::cout << "point q = ";
	std::cout << q.position.x << "  " << q.position.y << "  " << q.position.z << std::endl;

	dist = line.distLine2Point(q);
	std::cout << "dist from q to line = " << dist << std::endl;
	r = line.closestPointInLine(q);
	std::cout << "closest point to q in the line = " << std::endl;




	std::cout << r.position.x << "  " << r.position.y << "  " << r.position.z << std::endl;
	std::cout << "check point r is the line (bool) = " << line.isInside(r) << std::endl;
	std::cout << "check again point r is the line (dist) =" << line.distLine2Point(r) << std::endl;





	Line line2;
	line2.point = q;
	line2.setDirection(glm::vec3(0, 0, 1));	
	std::cout << "dist between lines = " << line.distLine2Line(line2) << std::endl;




	/// Test Plane methods
	Plane pla(glm::vec3(0), glm::vec3(1, 1, 1));
	pla.setPosition(glm::vec3(2, 2, 2));
	std::cout << "plane equation " << std::endl;
	std::cout << pla.normal.x << " .x + " << pla.normal.y << ".y +" << pla.normal.z << ".z + " << pla.dconst << "  = 0 " << std::endl;
	std::cout << "verifiquem que esta sobre el pla, bool= : " << pla.isInside(glm::vec3(2.0f)) << std::endl;

	q.setPosition(glm::vec3(0, 5, 6));
	dist = pla.distPoint2Plane(q.position);
	std::cout << "distancia des de q: " << dist << std::endl;
	r.setPosition(pla.closestPointInPlane(q.position)); //point in the plane
	std::cout << "r = " << r.position.x << "  " << r.position.y << "  " << r.position.z << std::endl;
	dist = pla.distPoint2Plane(r.position);
	std::cout << "verifiquem distancia: " << dist << std::endl;
	p.setPosition(-5, -6, 0);
	dist = pla.distPoint2Plane(p.position);
	std::cout << "distancia des de p: " << dist << std::endl;
	std::cout << "r = "<< r.position.x << "  " << r.position.y << "  " << r.position.z << std::endl;
	bool tall = pla.intersecSegment(p.position, q.position, r.position);
	std::cout << "verifiquem que esta sobre el pla, bool= : " << tall << std::endl;
	std::cout << "r = " << r.position.x << "  " << r.position.y << "  " << r.position.z << std::endl;
	dist = pla.distPoint2Plane(r.position);
	std::cout << "distancia des de r: " << dist << std::endl;

	system("pause");
	// Gestionar un vector d'estructures (per exemple tipus Point)
	const int nPunts = 1000000;
	std::cout << " " << std::endl;
	std::cout << "Numero de Punts = " << nPunts << std::endl;

	// Primer metode: vector dimensionat i assignem valors
	std::vector<Point> punts(nPunts);
	std::cout << " longitud del vector punts = " << punts.size() << std::endl;
	//Comptar el temps d'execuci�
	tini = tempsActual; //std::chrono::high_resolution_clock::now();
	for (int i = 0; i < nPunts; i++) {
		punts[i] = p;
	}
	tfin = tempsActual; //std::chrono::high_resolution_clock::now();
	duration = std::chrono::duration_cast<std::chrono::milliseconds>(tfin - tini).count();
	std::cout << "milliseconds = " << duration << std::endl;

	// Segon metode: vector dimensionat i push_back
	punts.clear();
	std::cout << " longitud del vector punts = " << punts.size() << std::endl;
	tini = tempsActual; // std::chrono::high_resolution_clock::now();
	for (int i = 0; i < nPunts; i++) {
		punts.push_back(p);
	}
	tfin = tempsActual; // std::chrono::high_resolution_clock::now();
	duration = std::chrono::duration_cast<std::chrono::milliseconds>(tfin - tini).count();
	std::cout << "milliseconds = " << duration << std::endl;

	// Tercer metode: vector NO dimensionat i push_back
	std::vector<Point> punts2;
	tini = tempsActual; //std::chrono::high_resolution_clock::now();
	for (int i = 0; i < nPunts; i++) {
		punts2.push_back(p);
	}
	std::cout << " longitud del vector punts = " << punts2.size() << std::endl;
	tfin = tempsActual; //std::chrono::high_resolution_clock::now();
	duration = std::chrono::duration_cast<std::chrono::milliseconds>(tfin - tini).count();
	std::cout << "milliseconds = " << duration << std::endl;*/

	
};