#pragma once
#include "Geometry.h"
#include <math.h>


//****************************************************
// Point
//****************************************************

Point::Point() {
};

Point::Point(const float& x, const float& y, const float& z) {
	position.x = x;
	position.y = y;
	position.z = z;
};

Point::Point(const glm::vec3& newPos) {
	position = newPos;
};

void Point::setPosition(const glm::vec3& newPos) {
	position = newPos;

};

void Point::setPosition(const float& x, const float& y, const float& z) {
	position.x = x;
	position.y = y;
	position.z = z;
};

float Point::distPoint2Point(const Point& punt) {
	float dist;
	return dist = glm::length(this->position - punt.position);
}

bool Point::isInside(const glm::vec3& point) { return false; };

Point Point::pointInSegment(const Point& q, const float& alfa) {
	Point r = (1 - alfa)*(this->position) + alfa*q.position;
	return r;
};


//****************************************************
// Line
//****************************************************

Line::Line()
{
};

Line::Line(const glm::vec3& origen, const glm::vec3& vector) {
	point.position = origen;
	direction = vector;
};

Line::Line(const Point& origen, const Point& extrem) {
	point.position = origen.position;
	direction = extrem.position - origen.position;
};

void Line::setPosition(const glm::vec3& newPos) {
	point.position = newPos;
};

void Line::setDirection(const glm::vec3& newDir) {
	direction = newDir;
};

bool Line::isInside(const Point& point) { //not yet implemented
	glm::vec3 v0 = point.position - this->point.position;
	glm::vec3 v1 = glm::cross(v0, direction);
	float distance = glm::length(v1) / glm::length(direction);
	if (abs(distance) <pow(10.0, -6.0))
	{
		return true;

	}
	else
	{
		return false;
	}
};

bool Line::isInside(const glm::vec3& punt) { //not yet implemented
	glm::vec3 v0 = punt - this->point.position;
	glm::vec3 v1 = glm::cross(v0, direction);
	float distance = glm::length(v1) / glm::length(direction);
	if (abs(distance) <pow(10.0, -6.0))
	{
		return true;

	}
	else
	{
		return false;
	}
};

float Line::distLine2Point(const Point& point) {
	glm::vec3 v0 = this->point.position - point.position;
	glm::vec3 v1 = glm::cross(v0, direction);
	float distance = glm::length(v1) / glm::length(direction);
	return distance;
};

float Line::projectPointToLine(const Point& point)
{
	float number = (glm::dot((point.position - this->point.position), this->direction) / (glm::dot(this->direction, this->direction)));
	return number;

}

glm::vec3 Line::closestPointInLine(const Point& point) {
	glm::vec3 v1 = this->point.position + (projectPointToLine(point) * this->direction);
	return v1;
};

float Line::distLine2Line(const Line& line) {
	glm::vec3 v0 = line.point.position - this->point.position;
	glm::vec3 v1 = glm::cross(line.direction, this->direction);
	float number = glm::dot(v0, v1);
	float number2 = glm::length(glm::cross(line.direction, this->direction));
	return number / number2;
};

//****************************************************
// Plane
//****************************************************

Plane::Plane(const glm::vec3& point, const glm::vec3& normalVect) {
	normal = glm::normalize(normalVect);
	dconst = -glm::dot(point, normal);
};

Plane::Plane(const glm::vec3& point0, const glm::vec3& point1, const glm::vec3& point2) {
	glm::vec3 v1 = point1 - point0;
	glm::vec3 v2 = point2 - point0;
	normal = glm::normalize(glm::cross(v1, v2));
	dconst = -glm::dot(point0, normal);
};

void Plane::setPosition(const glm::vec3& newPos) {
	dconst = -glm::dot(newPos, normal);
};

bool Plane::isInside(const glm::vec3& point) {
	/*float number = (point.x * normal.x + point.y * normal.y + point.z * normal.z + dconst);
	number = number / (sqrt((normal.x * normal.x) + (normal.y * normal.y) + (normal.z * normal.z)));*/
	float number = normal.x * point.x + normal.y * point.y + normal.z * point.z + dconst;
	if (abs(number) <pow(10.0, -6.0))
	{
		return true;
	}
	else
	{
		return false;
	}
};

float Plane::distPoint2Plane(const glm::vec3& point) { //return dist with sign

	float number = (point.x * normal.x + point.y * normal.y + point.z * normal.z + dconst);
	number = number / (sqrt((normal.x * normal.x) + (normal.y * normal.y) + (normal.z * normal.z)));
	return number;

};

glm::vec3 Plane::closestPointInPlane(const glm::vec3& point) {
	float alpha = ((-dconst - ((glm::dot(normal, point)))) / (glm::dot(normal, normal)));
	glm::vec3 v1 = point + alpha * normal;
	return v1;
};

bool Plane::intersecSegment(const glm::vec3& point1, const glm::vec3& point2, glm::vec3& pTall) {
	glm::vec3 v1;
	v1.x= abs(point2.x - point1.x);
	v1.y = abs(point2.y - point1.y);
	v1.z = abs(point2.z - point1.z);

	float alpha = ((-dconst - ((glm::dot(this->normal, point1)))) / (glm::dot(normal, v1)));
	//pTall = this->normal*(point1 + v1* alpha);
	if (alpha <= 1 && alpha >= 0)
	{
		pTall = point1 + alpha * v1;
		return true;
		//float pTall2 = pTall.x + pTall.y + pTall.z + this->dconst;

	/*	if (abs(pTall2) <pow(10.0, -6.0))
		{
			return true;

		}
		else
		{
			return false;

		}*/
	}
	else
	{
		return false;

	}
	



};

bool Plane::intersecLinePlane(const Line& line, glm::vec3& pTall) {

	float alpha = (-dconst - ((glm::dot(this->normal, line.point.position))) / (glm::dot(normal, line.direction)));
	pTall = this->normal*(line.point.position + line.direction * alpha);
	float pTall2 = pTall.x + pTall.y + pTall.z + this->dconst;

	if (pTall2 == 0)
	{
		return true;

	}
	else
	{
		return false;

	}
};


//****************************************************
// Triangle
//****************************************************

Triangle::Triangle(const glm::vec3& point0, const glm::vec3& point1, const glm::vec3& point2) {
	vertex1 = point0;
	vertex2 = point1;
	vertex3 = point2;
	normal = glm::normalize(glm::cross(vertex2 - vertex1, vertex3 - vertex2));
};

void Triangle::setPosition(const glm::vec3& newPos) {
	glm::vec3 baryc;
	baryc = (vertex1 + vertex2 + vertex3) / 3.0f;     //displacement of the barycenter 
	glm::vec3 disp = newPos - baryc; //is only considered
	vertex1 = vertex1 + disp;
	vertex2 = vertex2 + disp;
	vertex3 = vertex3 + disp;
};

bool Triangle::isInside(const glm::vec3& point) {

	//plano 4x-3y+5z-3 = 0
	glm::vec3 a1 = (glm::cross(vertex2 - point.x, vertex3 - point.x));
	float s1 = (sqrt((a1.x * a1.x) + (a1.y *a1.y) + (a1.z * a1.z))) / 2;
	glm::vec3 a2 = (glm::cross(point.x - vertex1, vertex3 - vertex1));
	float s2 = (sqrt((a2.x * a2.x) + (a2.y *a2.y) + (a2.z * a2.z))) / 2;
	glm::vec3 a3 = (glm::cross(vertex2 - vertex1, point.x - vertex1));
	float s3 = (sqrt((a3.x * a3.x) + (a3.y *a3.y) + (a3.z * a3.z))) / 2;
	glm::vec3 a4 = (glm::cross(vertex2 - vertex1, vertex3 - vertex1));
	float s4 = (sqrt((a4.x * a4.x) + (a4.y *a4.y) + (a4.z * a4.z))) / 2;
	if (s1 + s2 + s3 - s4 == 0)
	{
		return true;

	}
	else
	{
		return false;
	}
};

bool Triangle::intersecSegment(const glm::vec3& point1, const glm::vec3& point2, glm::vec3& pTall) {
	return true;
};

//****************************************************
// Sphere
//****************************************************

Sphere::Sphere(const glm::vec3& point, const float& radious) {
	center = point;
	radi = radious;
};

void Sphere::setPosition(const glm::vec3& newPos) {
	center = newPos;
};

bool Sphere::isInside(const glm::vec3& point) {
	float dist = glm::length(point - center);
	if (dist > radi) return false;
	return true;
};

bool Sphere::intersecSegment(const glm::vec3& point1, const glm::vec3& point2, glm::vec3& pTall) {
	glm::vec3 v(point2.x - point1.x, point2.y - point1.y, point2.z - point1.z);

	float alpha = glm::dot(v, v);
	float beta = glm::dot((v + v), (point1 - this->center));
	float gamma = glm::dot(this->center, this->center) + glm::dot(point1, point1) - glm::dot((point1 + point1), this->center) - (this->radi * this->radi);

	float a = alpha;
	float b = beta;
	float c = gamma;
	int n = 2;
	float k1;
	float k2;

	k1 = (-b + (sqrt(pow(b, n) - (4 * a * c)))) / (2 * a);
	k2 = (-b - (sqrt(pow(b, n) - (4 * a * c)))) / (2 * a);

	if ((k1 <= 1) && (k1 >= 0)) 
	{
		if (k2 <= 1 && k2 >= 0)
		{
			if (k1 < k2)
			{
				pTall.x = point1.x + k1 * v.x;
				pTall.y = point1.y + k1 * v.y;
				pTall.z = point1.z + k1 * v.z;
				return true;
			}
			else if (k2 < k1)
			{
				pTall.x = point1.x + k2 * v.x;
				pTall.y = point1.y + k2 * v.y;
				pTall.z = point1.z + k2 * v.z;
				return true;
			}
		}
		else
		{
			pTall.x = point1.x + k1 * v.x;
			pTall.y = point1.y + k1 * v.y;
			pTall.z = point1.z + k1 * v.z;
			return true;
		}

		return true;
	}
	else if ((k2 <= 1) && (k2 >= 0))
	{
		pTall.x = point1.x + k2 * v.x;
		pTall.y = point1.y + k2 * v.y;
		pTall.z = point1.z + k2 * v.z;
		return true;
	}
	else return false;

};