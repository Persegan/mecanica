/***************************************************************
* Tutorial for understanding the glm vector matrix lib. 
* glm is supposed to be copied as a folder in the corresponding 
* include directory (in our case .\..\deps\include)
*
* (c) Toni Susin 2016
**************************************************************
*/


#pragma once
#include <iostream>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>

//Usefull macros for printing vectors and matrices
#define PRINT_VEC3(_v) std::cout << _v.x << " " << _v.y << " " << _v.z << std::endl;
#define PRINT_MAT3(_M) PRINT_VEC3(glm::vec3(glm::vec3(1, 0, 0)*_M)) PRINT_VEC3(glm::vec3(glm::vec3(0,1,0)*_M)) PRINT_VEC3(glm::vec3(glm::vec3(0,0,1)*_M))
#define PRINT_VEC4(_v) std::cout << _v[0] << " " << _v[1]<< " " << _v[2] << " " << _v[3] << std::endl;
#define PRINT_MAT4(_M) PRINT_VEC4(glm::vec4(glm::vec4(1,0,0,0)*_M)) PRINT_VEC4(glm::vec4(glm::vec4(0,1,0,0)*_M)) PRINT_VEC4(glm::vec4(glm::vec4(0,0,1,0)*_M)) PRINT_VEC4(glm::vec4(glm::vec4(0,0,0,1)*_M)) 

//Macro for generating a random value between 0 and 1
#define RANDOM01 ( float(rand()) / float(RAND_MAX) )


void main(){
	glm::vec3 pos = glm::vec3(1); //vector 3 constant = 1
	std::cout << "pos: "; 
	PRINT_VEC3(pos); //macro for printing on console
	glm::vec4 color = glm::vec4(0.5, 0.8, 0.9, 1.0); //color RGBA 4-dim vector
	std::cout << "color: "; PRINT_VEC4(color);

	glm::mat3 m = glm::mat3(
		1.1, 2.1, 3.1, // first column (not row!)
		1.2, 2.2, 3.2, // second column
		1.3, 2.3, 3.3  // third column
		);
	std::cout << "3x3 matrix m: " << std::endl;	PRINT_MAT3(m);
	std::cout << "determinant m:  " << glm::determinant(m) << std::endl;

	system("pause");





	//it is a singular matrix
	//modify for having a non-singular one (adding 5 to the diagonal)
	m = m + glm::mat3(5);
	std::cout << "new m: " << std::endl; PRINT_MAT3(m);
	std::cout << "determinant m: " << glm::determinant(m) << std::endl;
	glm::mat3 mInv = glm::inverse(m);
	std::cout << "mInv: " << std::endl; PRINT_MAT3(mInv);
	std::cout << "m * mInv: " << std::endl; PRINT_MAT3(m*mInv);

	system("pause");




	//Vector operations:
	glm::vec3 v1, v2, v3;
	v1 = glm::vec3(1.0f, 2.0f, 3.0f);
	v2 = glm::vec3(RANDOM01, RANDOM01, RANDOM01);
	std::cout << "v2 = "; PRINT_VEC3(v2);
	std::cout << "v1 = "; PRINT_VEC3(v1);
	//sum of v1 + v2
	v3 = v1 + v2;
	std::cout << "sum of v1 + v2 = "; PRINT_VEC3(v3);

	//length of a vector ||v||
	std::cout << "length v1: " << glm::length(v1) << std::endl;
	//normalize a vector  v/||v||
	v2 = glm::normalize(v2);
	std::cout << "v2 normalized = "; PRINT_VEC3(v2);
	//dot product v1�v2
	std::cout << "dor product " << glm::dot(v1, v2) << std::endl;
	//angle between two vectors
	float ang = acos(glm::dot(v1, v2) / (glm::length(v1) * glm::length(v2)));
	std::cout << "angle between v1, v2:  " << ang << " ... or in degrees: " << glm::degrees(ang) << std::endl;
	//cross product v1 x v2
	v3 = glm::cross(v1, v2);
	std::cout << "cross product v1xv2 "; PRINT_VEC3(v3);





	//geometrical operations
	glm::mat4 mMat = glm::mat4(); // matriu 4 x 4 identitat
	std::cout << "mMat: " << std::endl; PRINT_MAT4(mMat);

	glm::mat4 mMatTranslated = glm::translate(mMat, pos);
	std::cout << "mMatTranslate by pos: " << std::endl; PRINT_MAT4(mMatTranslated);

	glm::mat4 mMatScaled = glm::scale(mMat, glm::vec3(2));
	std::cout << "mMatScaled by 2: " << std::endl; PRINT_MAT4(mMatScaled);


	float angZ = 45, angY = 30, angX = 60;
	glm::mat4 mMatRotX = glm::rotate(glm::mat4(1.0f), glm::radians(angX), glm::vec3(1.0f, 0.0f, 0.0f));
	std::cout << "angX = " << angX << " mMatRotX: " << std::endl; PRINT_MAT4(mMatRotX);
	glm::mat4 mMatRotY = glm::rotate(glm::mat4(1.0f), glm::radians(angY), glm::vec3(1.0f, 0.0f, 0.0f));
	std::cout << "angY = " << angY << " mMatRotY: " << std::endl; PRINT_MAT4(mMatRotY);
	glm::mat4 mMatRotZ = glm::rotate(glm::mat4(1.0f), glm::radians(angZ), glm::vec3(1.0f, 0.0f, 0.0f));
	std::cout << "angZ = " << angZ << " mMatRotZ: " << std::endl; PRINT_MAT4(mMatRotZ);
	system("pause");

	//rotation of a vector
	glm::vec4 vv(glm::vec3(1), 0.0);
	vv = mMatRotZ*vv;
	std::cout << "Rotate a vector with mMatRotZ: " << std::endl; PRINT_VEC4(vv);

	system("pause");


}