#include "Game.h"

Game::Game(std::string windowTitle, int screenWidth, int screenHeight, int maxFPS) :
	_windowTitle(windowTitle), _screenWidth(screenWidth), _screenHeight(screenHeight),_gameState(GameState::INIT), _maxFPS(maxFPS) {

}

Game::~Game()
{
}

/*
* Game execution
*/
void Game::run() {
		//System initializations
	initSystems();
		//Start the game if all the elements are ready
	gameLoop();
	//system("pause"); //Don't close the command window
}

/*
* Initializes all the game engine components
*/
void Game::initSystems() {
		//Create an Opengl window using SDL
	_window.create(_windowTitle, _screenWidth, _screenHeight, 0);
		//Set the max fps
	_fpsLimiter.setMaxFPS(_maxFPS);
		//Compile and Link shader
	loadShaders();
		//Load all the Game Objects
	loadGameObjects(NumGameObj);
	_firsttime = true;
		//Load and prepare the textures
	//loadTextures();	
}

/*
* Compiles, sets the variables between C++ and the Shader program and links the shader program
*/
void Game::loadShaders() {
		//Compile the shaders
	_glProgram.addShader(GL_VERTEX_SHADER, "./vertex-shader.txt");
	_glProgram.addShader(GL_FRAGMENT_SHADER, "./fragment-shader.txt");
	_glProgram.compileShaders();
		//Attributes must be added before linking the code
	_glProgram.addAttribute("vert");
		//Link the compiled shaders
	_glProgram.linkShaders();
}

/*
* Loads all the element that has to be displayed on the screen
*/
void Game::loadGameObjects(const int& NumGameObj) {
	// make and bind the VAO
	
	glGenVertexArrays(NumGameObj, &gVAO[0]);
	
	//----------------------
	// LOAD the Triangle
	//----------------------

	// bind the VAO for the Triangle
	glBindVertexArray(gVAO[0]);
	// make and bind the VBO
	glGenBuffers(1, &gVBO[0]);
	glBindBuffer(GL_ARRAY_BUFFER, gVBO[0]);

	// define vertex coordinates
	std::vector<glm::vec3> vertexData;
	float mida = 0.2f;
//	float altura = sin(M_PI / 4.0f) * (2 * mida);
	float altura = sqrt(2.0f) * mida;
	vertexData.push_back(glm::vec3(-mida+0.1f, 0.0f, 0.0f));
	vertexData.push_back(glm::vec3(mida+0.1f, 0.0f, 0.0f));
	vertexData.push_back(glm::vec3(0.1f, altura, 0.0f));


	//for (int i = 0; i < 3; i++) {
	//	vertexData[i] = vertexData[i] + glm::vec3(0.4f, 0.4f, 0.0f); //translate to point (0.4,0.4), no matter about z value. 
	//}
	glBufferData(GL_ARRAY_BUFFER, vertexData.size()*sizeof(glm::vec3), &vertexData[0], GL_STATIC_DRAW);
	
	// connect the xyz to the "vert" attribute of the vertex shader
	glEnableVertexAttribArray(_glProgram.getAttribLocation("vert"));
	glVertexAttribPointer(_glProgram.getAttribLocation("vert"), 3, GL_FLOAT, GL_FALSE, 0, NULL);

	// unbind the VBO and VAO
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//Here we need to make and load the circle and or rectangles

	glBindVertexArray(gVAO[2]);

	glGenBuffers(1, &gVBO[2]);
	glBindBuffer(GL_ARRAY_BUFFER, gVBO[2]);

	// define vertex coordinates
	std::vector<glm::vec3> vertexData2;
	int i;
	int lineAmount = 100; //# of triangles used to draw circle
	float center_x = -0.2;
	float center_y = -0.4;
	float radius = 0.1;
	float twicePi = 2 * 3.141592;
	for (i = 0; i <= lineAmount; i++) {
		vertexData2.push_back(glm::vec3(
			center_x + (radius * cos(i *  twicePi / lineAmount)),
			center_y + (radius* sin(i * twicePi / lineAmount)), 0.0f)
			);
	}
	glEnd();

	glBufferData(GL_ARRAY_BUFFER, vertexData2.size()*sizeof(glm::vec3), &vertexData2[0], GL_STATIC_DRAW);

	// connect the xyz to the "vert" attribute of the vertex shader
	glEnableVertexAttribArray(_glProgram.getAttribLocation("vert"));
	glVertexAttribPointer(_glProgram.getAttribLocation("vert"), 3, GL_FLOAT, GL_FALSE, 0, NULL);

	// unbind the VBO and VAO
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);



	//Let's try to draw a second sphere
	std::vector<glm::vec3> vertexData3;
	glBindVertexArray(gVAO[3]);

	glGenBuffers(1, &gVBO[3]);
	glBindBuffer(GL_ARRAY_BUFFER, gVBO[3]);

	// define vertex coordinates
	lineAmount = 100; //# of triangles used to draw circle
	center_x = -0.4;
	center_y = -0.6;
	radius = 0.1;
	for (i = 0; i <= lineAmount; i++) {
		vertexData3.push_back(glm::vec3(
			center_x + (radius * cos(i *  twicePi / lineAmount)),
			center_y + (radius* sin(i * twicePi / lineAmount)), 0.0f)
			);
	}
	glEnd();

	glBufferData(GL_ARRAY_BUFFER, vertexData3.size()*sizeof(glm::vec3), &vertexData3[0], GL_STATIC_DRAW);

	// connect the xyz to the "vert" attribute of the vertex shader
	glEnableVertexAttribArray(_glProgram.getAttribLocation("vert"));
	glVertexAttribPointer(_glProgram.getAttribLocation("vert"), 3, GL_FLOAT, GL_FALSE, 0, NULL);

	// unbind the VBO and VAO
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);


	


	//-------------------------------------
	// INIT and LOAD the Particle System
	//-------------------------------------
	
	// make and bind the VAO
	glBindVertexArray(gVAO[1]);
	// make and bind the VBO
	glGenBuffers(1, &gVBO[1]);
	glBindBuffer(GL_ARRAY_BUFFER, gVBO[1]);
	
	sysParticles.resize(_Numparticles);
	posSysPart.resize(_Numparticles);
	for (int i = 0; i < _Numparticles; i++) {
		// Initialize Particles
		sysParticles[i].setPosition(-0.5f + (0.03f*i), 0.5f, 0.0f);
		sysParticles[i].setVelocity(3.2f, 3.0f, 0.0f);
		//sysParticles[0].setVelocity(0.2f, 0.5f, 0.0f);//
		sysParticles[i].setLifetime(50.0f);
		sysParticles[i].setBouncing(0.99f);
		sysParticles[i].setFixed(false);
		sysParticles[i].setPreviousPosition(sysParticles[i].getCurrentPosition() - sysParticles[i].getVelocity() * _dt);

		posSysPart[i] = sysParticles[i].getCurrentPosition(); //Copy position values
	}
	//sysParticles[0].setFixed(true);
	

	
	//Initialize Rope Particles
	/*for (int i = 0; i < 5; i++) {

		rope[i].setPosition(0.1f+(0.1f*i), 0.5f, 0.0f);
		rope[i].setVelocity(4.0f, 3.0f, 0.0f);
		//sysParticles[0].setVelocity(0.2f, 0.5f, 0.0f);//
		rope[i].setLifetime(50.0f);
		rope[i].setBouncing(0.99f);
		rope[i].setFixed(false);
		posSysPart[i+1] = rope[i].getCurrentPosition();
	}*/

//	glBufferData(GL_ARRAY_BUFFER, sizeof(posSysPart), posSysPart, GL_STATIC_DRAW);
	glBufferData(GL_ARRAY_BUFFER, posSysPart.size()*sizeof(glm::vec3), &posSysPart[0], GL_STREAM_DRAW);
	// connect the xyz to the "vert" attribute of the vertex shader
	glEnableVertexAttribArray(_glProgram.getAttribLocation("vert"));
	glVertexAttribPointer(_glProgram.getAttribLocation("vert"), 3, GL_FLOAT, GL_FALSE, 0, NULL);

	// unbind the VBO and VAO
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	//----------------------
	// LOAD the Botton-Plane and the figures to detect collisions
	//----------------------
	_planeBottom.setPointNormal(glm::vec3(0.0f, -1.0f, 0.0f),glm::vec3(0.0f, -1.0f, 0.0f));
	_planeTop.setPointNormal(glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	_planeLeft.setPointNormal(glm::vec3(-1.0f, 0.0f, 0.0f), glm::vec3(-1.0f, 0.0f, 0.0f));
	_planeRight.setPointNormal(glm::vec3(1.0f, 0.0f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	
	_polTriang.addVertexPositions(glm::vec3(-mida + 0.1f, 0.0f, 0.0f));
	_polTriang.addVertexPositions(glm::vec3(mida + 0.1f, 0.0f, 0.0f));
	_polTriang.addVertexPositions(glm::vec3(0.1f, altura, 0.0f));
	_numOfElements = _polTriang.getNumElements();

	_sphere.center = glm::vec3(-0.2f, -0.4f, 0.0f);
	_sphere.radi = 0.1f;

	_sphere2.center = glm::vec3(-0.4f, -0.6f, 0.0f);
	_sphere2.radi = 0.1f;
	
}

/*
* Loads and prepares the textures
*/
//void Game::loadTextures() {
	//	
//}


/*
* Game execution: Gets input events, processes game logic and draws objects on the screen
*/
void Game::gameLoop() {
	float _fps;
	int frameCounter = 0;
	_gameState = GameState::PLAY;
	//float time = 0.0f;
	while (_gameState != GameState::EXIT) {
			//Start the fps limiter for the current frame
		_fpsLimiter.begin();
			//Process the input information (keyboard and mouse)
		processInput();
			//Execute actions based on the input events
		executeActions();
		_time = _time + _dt;
		std::cout << "Total time = " << _time << std::endl;
			//Draw the objects on the screen
		drawGame();			
			//Delay (or not) the execution for allowing the expected FPS
		_fps = _fpsLimiter.end();
			//Draw the current FPS in the console
		if (frameCounter == 10) {
			cout << "FPS:" << _fps << endl;
			frameCounter = 0;
		}
		frameCounter++;
	}
}


/*
* Processes input with SDL
*/
void Game::processInput() {
	//Review https://wiki.libsdl.org/SDL_Event to see the different kind of events
	//Moreover, table show the property affected for each event type
	SDL_Event evnt;

		//Update the input event states. Current states are moved to the previous states for being able to detect pressed keys
	_inputManager.update();

	//Will keep looping until there are no more events to process
	while (SDL_PollEvent(&evnt)) {
		switch (evnt.type) {
		case SDL_QUIT:
			_gameState = GameState::EXIT;
			break;
		case SDL_MOUSEMOTION:
			_inputManager.setMouseCoords(evnt.motion.x, evnt.motion.y);
			break;
		case SDL_KEYDOWN:
			_inputManager.pressKey(evnt.key.keysym.sym);
			break;
		case SDL_KEYUP:
			_inputManager.releaseKey(evnt.key.keysym.sym);
			break;
		case SDL_MOUSEBUTTONDOWN:
			_inputManager.pressKey(evnt.button.button);
			break;
		case SDL_MOUSEBUTTONUP:
			_inputManager.releaseKey(evnt.button.button);
			break;
		default:
			break;
		}
	}
}

/**
* Moves the objects based on their logics and the input events
*/
void Game::executeActions() {

	if (_inputManager.isKeyPressed(SDL_BUTTON_LEFT)){
		glm::ivec2 mouseCoords = _inputManager.getMouseCoords();
		//mouseCoords = _camera.convertScreenToWorld(mouseCoords);
		cout << mouseCoords.x << ", " << mouseCoords.y << endl;
	}

	float disact, disant;
	float disactTop, disantTop;
	float disactLeft, disantLeft;
	float disactRight, disantRight;

	std::cout << sysParticles[0].getCurrentPosition().x << std::endl;
	std::cout << sysParticles[0].getCurrentPosition().y << std::endl;;
	std::cout << sysParticles[0].getCurrentPosition().z << std::endl;;

	
	for (int j = 0; j < _Numparticles; j++)
	{
		int interTrian = 0;
		glm::vec3 posAnt = sysParticles[j].getCurrentPosition();
		glm::vec3 posAct;

		disant = _planeBottom.distPoint2Plane(sysParticles[j].getCurrentPosition());
		disantTop = _planeTop.distPoint2Plane(sysParticles[j].getCurrentPosition());
		disantLeft = _planeLeft.distPoint2Plane(sysParticles[j].getCurrentPosition());
		disantRight = _planeRight.distPoint2Plane(sysParticles[j].getCurrentPosition());
	//	float center_x = -0.2;
	//	float center_y = -0.4;
	//	float radius = 0.1;


		sysParticles[j].setForce(0.0f, 0.0f, 0.0f);  //Avoid to accumulate
		sysParticles[j].addForce(0.0f, -9.8f, 0.0f); //gravity
		if (sysParticles[j].getHasCollided() == true)
		{
			sysParticles[j].setPreviousPosition(sysParticles[j].getCurrentPosition()- sysParticles[j].getVelocity() * _dt);
			sysParticles[j].setHasCollided();
		}
		sysParticles[j].updateParticle(_dt, Particle::UpdateMethod::Verlet);
		//Check for floor collisions
		sysParticles[j].setLifetime(sysParticles[j].getLifetime() - _dt); //lifetime is decreased
		disact = _planeBottom.distPoint2Plane(sysParticles[j].getCurrentPosition());
		disactTop = _planeTop.distPoint2Plane(sysParticles[j].getCurrentPosition());
		disactLeft = _planeLeft.distPoint2Plane(sysParticles[j].getCurrentPosition());
		disactRight = _planeRight.distPoint2Plane(sysParticles[j].getCurrentPosition());

		posAct = sysParticles[j].getCurrentPosition();
	

		if (sysParticles[j].getLifetime() > 0) {
			for (int i = 0; i < 3; i++) {
				float alpha = (((sysParticles[j].getCurrentPosition().y) - (_polTriang._vertexVector[i % 3].y)) / ((_polTriang._vertexVector[(i + 1) % 3].y) - (_polTriang._vertexVector[(i) % 3].y)));
				if (alpha >= 0 && alpha <= 1) {
					glm::vec3 Segmento = (1 - alpha) * _polTriang._vertexVector[i % 3] + alpha * _polTriang._vertexVector[(i + 1) % 3];
					if (Segmento.x >= sysParticles[j].getCurrentPosition().x) {
						interTrian++;
					}
				}
			}
			int ALPHO;

			if (interTrian % 2 == 1) {
				std::cout << "hji";
				glm::vec3 n(0);
				glm::vec3 collisionVector(0);
				float d;
				for (int i = 0; i < 3; i++) {

					if (i < (3 - 1)) collisionVector = _polTriang._vertexVector[(i + 1) % 3] - _polTriang._vertexVector[i];
					else if (i == (3 - 1)) collisionVector = _polTriang._vertexVector[0] - _polTriang._vertexVector[i];

					n = (glm::vec3(collisionVector.y, -collisionVector.x, 0) / glm::length(collisionVector));
					d = glm::dot(-n, _polTriang._vertexVector[i]);
					ALPHO = (-d - glm::dot(n, sysParticles[j].getPreviousPosition())) / (glm::dot(n, sysParticles[j].getCurrentPosition() - sysParticles[j].getPreviousPosition()));

					if (ALPHO >= 0 && ALPHO <= 1) {
						cout << "ciedf!";
						glm::vec3 v = _polTriang._vertexVector[(i + 1) % 3] - _polTriang._vertexVector[i];
						n = glm::vec3(v.y, -v.x, 0) / glm::length(v);
						d = glm::dot(-n, _polTriang._vertexVector[i]);
						sysParticles[j].setPosition(sysParticles[j].getCurrentPosition() - (1 + sysParticles[j].getBouncing())* (glm::dot(n, sysParticles[j].getCurrentPosition()) + d) * n);
						sysParticles[j].setVelocity(sysParticles[j].getVelocity() - (1 + sysParticles[j].getBouncing())*(glm::dot(n, sysParticles[j].getVelocity()))*n);
						sysParticles[j].setHasCollided();
						//glm::vec3 correcPos = -(1 + sysParticles[0].getBouncing())*(glm::dot(n,sysParticles[0].getCurrentPosition()) + d)* n;
						//glm::vec3 correcVel = -(1 + sysParticles[0].getBouncing())*(n*sysParticles[0].getVelocity())*n;
						//sysParticles[0].setPosition(sysParticles[0].getPreviousPosition() + correcPos);
						//sysParticles[0].setVelocity(sysParticles[0].getVelocity() + correcVel);
						break;
					}
				}

			}



			if (disant*disact < 0.0f) {
				//only valid for the plane y=0 (floor plane)
				glm::vec3 correcPos = -(1 + sysParticles[j].getBouncing()) * disact *_planeBottom.normal;
				glm::vec3 correcVel = -(1 + sysParticles[j].getBouncing()) * (sysParticles[j].getVelocity()*_planeBottom.normal)*_planeBottom.normal;
				sysParticles[j].setPosition(sysParticles[j].getCurrentPosition() + correcPos);
				sysParticles[j].setVelocity(sysParticles[j].getVelocity() + correcVel);
				sysParticles[j].setHasCollided();
			}
			if (disantTop*disactTop < 0.0f) {
				//only valid for the plane y=0 (floor plane)
				glm::vec3 correcPos = -(1 + sysParticles[j].getBouncing()) * disactTop *_planeTop.normal;
				glm::vec3 correcVel = -(1 + sysParticles[j].getBouncing()) * (sysParticles[j].getVelocity()*_planeTop.normal)*_planeTop.normal;
				sysParticles[j].setPosition(sysParticles[j].getCurrentPosition() + correcPos);
				sysParticles[j].setVelocity(sysParticles[j].getVelocity() + correcVel);
				sysParticles[j].setHasCollided();
			}
			if (disantLeft*disactLeft < 0.0f) {
				//only valid for the plane y=0 (floor plane)
				glm::vec3 correcPos = -(1 + sysParticles[j].getBouncing()) * disactLeft *_planeLeft.normal;
				glm::vec3 correcVel = -(1 + sysParticles[j].getBouncing()) * (sysParticles[j].getVelocity()*_planeLeft.normal)*_planeLeft.normal;
				sysParticles[j].setPosition(sysParticles[j].getCurrentPosition() + correcPos);
				sysParticles[j].setVelocity(sysParticles[j].getVelocity() + correcVel);
				sysParticles[j].setHasCollided();
			}
			if (disantRight*disactRight < 0.0f) {
				//only valid for the plane y=0 (floor plane)
				glm::vec3 correcPos = -(1 + sysParticles[j].getBouncing()) * disactRight *_planeRight.normal;
				glm::vec3 correcVel = -(1 + sysParticles[j].getBouncing()) * (sysParticles[j].getVelocity()*_planeRight.normal)*_planeRight.normal;
				sysParticles[j].setPosition(sysParticles[j].getCurrentPosition() + correcPos);
				sysParticles[j].setVelocity(sysParticles[j].getVelocity() + correcVel);
				sysParticles[j].setHasCollided();
			}

			//Detectar y corregir colisi�n con la esfera
			if (glm::length(posAct - _sphere.center) < _sphere.radi)
			{
				//Encontramos el plano con el que tenemos que rebotar
				glm::vec3 CutPoint;
				_sphere.intersecSegment(posAct, posAnt, CutPoint);
				Plane CollisionPlane(CutPoint, CutPoint - _sphere.center);

				glm::vec3 n = CollisionPlane.normal;
				float d = CollisionPlane.dconst;
				sysParticles[j].setPosition(sysParticles[j].getCurrentPosition() - (1 + sysParticles[j].getBouncing())* (glm::dot(n, sysParticles[j].getCurrentPosition()) + d) * n);
				sysParticles[j].setVelocity(sysParticles[j].getVelocity() - (1 + sysParticles[j].getBouncing())*(glm::dot(n, sysParticles[j].getVelocity()))*n);
				sysParticles[j].setHasCollided();
			}

			if (glm::length(posAct - _sphere2.center) < _sphere2.radi)
			{
				//Encontramos el plano con el que tenemos que rebotar
				glm::vec3 CutPoint;
				_sphere2.intersecSegment(posAct, posAnt, CutPoint);
				Plane CollisionPlane(CutPoint, CutPoint - _sphere2.center);

				glm::vec3 n = CollisionPlane.normal;
				float d = CollisionPlane.dconst;
				sysParticles[j].setPosition(sysParticles[j].getCurrentPosition() - (1 + sysParticles[j].getBouncing())* (glm::dot(n, sysParticles[j].getCurrentPosition()) + d) * n);
				sysParticles[j].setVelocity(sysParticles[j].getVelocity() - (1 + sysParticles[j].getBouncing())*(glm::dot(n, sysParticles[j].getVelocity()))*n);
				sysParticles[j].setHasCollided();
			}


		}
	}

		for (int i = 0; i < _Numparticles; i++){

	glm::vec3 posicio=sysParticles[i].getCurrentPosition();
	posSysPart[i] = posicio;
	// Pass to OpenGL
	glBindBuffer(GL_ARRAY_BUFFER, gVBO[1]);
	glBufferData(GL_ARRAY_BUFFER, posSysPart.size()*sizeof(glm::vec3), &posSysPart[0], GL_STREAM_DRAW);
	// connect the xyz to the "vert" attribute of the vertex shader
	glEnableVertexAttribArray(_glProgram.getAttribLocation("vert"));
	glVertexAttribPointer(_glProgram.getAttribLocation("vert"), 3, GL_FLOAT, GL_FALSE, 0, NULL);
	// unbind the VBO and VAO
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
}



/**
* Draw objects on the screen
*/
void Game::drawGame() {
	//Set the base depth to 1.0
	glClearDepth(1.0);

	//Clear the color and depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Bind the GLSL program. Only one code GLSL can be used at the same time
	_glProgram.use();

	/////////
	// bind the VAO (the triangle)
	glBindVertexArray(gVAO[0]);
	// draw the VAO
	glDrawArrays(GL_TRIANGLES, 0, 3);
	// unbind the VAO
	glBindVertexArray(0);


	glBindVertexArray(gVAO[2]);
	// draw the VAO
	glDrawArrays(GL_LINE_LOOP, 0, 100);
	// unbind the VAO
	glBindVertexArray(0);

	glBindVertexArray(gVAO[3]);
	// draw the VAO
	glDrawArrays(GL_LINE_LOOP, 0, 100);
	// unbind the VAO
	glBindVertexArray(0);


	// bind the VAO (the Particle)
	glBindVertexArray(gVAO[1]);
	// draw the VAO
	glPointSize(6);
	glDrawArrays(GL_POINTS, 0, _Numparticles);
	// unbind the VAO
	glBindVertexArray(0);

	/////////

		//Unbind the program
	_glProgram.unuse();

		//Swap the display buffers (displays what was just drawn)
	_window.swapBuffer();
}


/*
verlet

(x,x*)
x' = 2x -x* + a . At^2

x* = x*/