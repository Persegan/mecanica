#pragma once
#include "GeometryPlane.h"

//****************************************************
// Plane
//****************************************************

Plane::Plane(const glm::vec3& point, const glm::vec3& normalVect){
	normal = glm::normalize(normalVect);
	dconst = -glm::dot(point, normal);
};

Plane::Plane(const glm::vec3& point0, const glm::vec3& point1, const glm::vec3& point2){
	glm::vec3 v1 = point1 - point0;
	glm::vec3 v2 = point2 - point0;
	normal = glm::normalize(glm::cross(v1, v2));
	dconst = -glm::dot(point0, normal);
};

void Plane::setPosition(const glm::vec3& newPos) {
	dconst = -glm::dot(newPos, normal);
};

void Plane::setPointNormal(const glm::vec3& punt, const glm::vec3& normalVect) {
	normal = glm::normalize(normalVect);
	dconst = -glm::dot(punt, normal);
};

bool Plane::isInside(const glm::vec3& point){
	float dist;
	dist = glm::dot(point, normal) + dconst;
	if (dist > 1.e-7)
		return false;
	else
		return true;
};

float Plane::distPoint2Plane(const glm::vec3& point){
	float dist;
	return dist = glm::dot(point, normal) + dconst;
};

glm::vec3 Plane::closestPointInPlane(const glm::vec3& point){
	glm::vec3 closestP;
	float r = (-dconst - glm::dot(point, normal));
	return closestP = point + r*normal;
};

bool Plane::intersecSegment(const glm::vec3& point1, const glm::vec3& point2, glm::vec3& pTall){
	if (distPoint2Plane(point1)*distPoint2Plane(point2) > 0)	return false;
	float r = (-dconst - glm::dot(point1, normal)) / glm::dot((point2 - point1), normal);
	pTall = (1 - r)*point1 + r*point2;
	return true;
};


Polygon::Polygon(int elementsNum) { _elementsNum = elementsNum; };


void Polygon::addVertexPositions(glm::vec3 vertexPos) {
	_vertexVector.push_back(vertexPos);
	_elementsNum++;
};


//****************************************************
// Sphere
//****************************************************

Sphere::Sphere(const glm::vec3& point, const float& radious) {
	center = point;
	radi = radious;
};

void Sphere::setPosition(const glm::vec3& newPos) {
	center = newPos;
};

bool Sphere::isInside(const glm::vec3& point) {
	float dist = glm::length(point - center);
	if (dist > radi) return false;
	return true;
};

bool Sphere::intersecSegment(const glm::vec3& point1, const glm::vec3& point2, glm::vec3& pTall) {
	glm::vec3 v(point2.x - point1.x, point2.y - point1.y, point2.z - point1.z);

	float alpha = glm::dot(v, v);
	float beta = glm::dot((v + v), (point1 - this->center));
	float gamma = glm::dot(this->center, this->center) + glm::dot(point1, point1) - glm::dot((point1 + point1), this->center) - (this->radi * this->radi);

	float a = alpha;
	float b = beta;
	float c = gamma;
	int n = 2;
	float k1;
	float k2;

	k1 = (-b + (sqrt(pow(b, n) - (4 * a * c)))) / (2 * a);
	k2 = (-b - (sqrt(pow(b, n) - (4 * a * c)))) / (2 * a);

	if ((k1 <= 1) && (k1 >= 0))
	{
		if (k2 <= 1 && k2 >= 0)
		{
			if (k1 < k2)
			{
				pTall.x = point1.x + k1 * v.x;
				pTall.y = point1.y + k1 * v.y;
				pTall.z = point1.z + k1 * v.z;
				return true;
			}
			else if (k2 < k1)
			{
				pTall.x = point1.x + k2 * v.x;
				pTall.y = point1.y + k2 * v.y;
				pTall.z = point1.z + k2 * v.z;
				return true;
			}
		}
		else
		{
			pTall.x = point1.x + k1 * v.x;
			pTall.y = point1.y + k1 * v.y;
			pTall.z = point1.z + k1 * v.z;
			return true;
		}

		return true;
	}
	else if ((k2 <= 1) && (k2 >= 0))
	{
		pTall.x = point1.x + k2 * v.x;
		pTall.y = point1.y + k2 * v.y;
		pTall.z = point1.z + k2 * v.z;
		return true;
	}
	else return false;

};