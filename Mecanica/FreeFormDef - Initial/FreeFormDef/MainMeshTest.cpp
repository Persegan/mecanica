#pragma once
#include "mesh.h"
#include <iostream>
#include <chrono>
#include <math.h>

/**
 MeshTest:
 1. Load an OBJ file, 
 2. Modify some of the vertex coordinates 
 3. Write the modified mesh in a new OBJ file
*/

void main() {
	const int HEIGHT = 10;
	const int WIDTH = 10;
	const int DEPTH = 10;
	std::string name = "teapot"; 
	std::string fileIn = name + ".obj";
	std::string fileOut = name + "Mod.obj";

	Mesh mesh;
	std::vector<glm::vec3> vertexData;
	std::vector<glm::i32vec3> faceData;

	//-----------------------------------------------------
	//  Load a 3D mesh from an OBJ file, 
	//  modify the position of the vertex
	//  and write a new OBJ file.
	//-----------------------------------------------------

	mesh.loadObjModel(fileIn);//load a 3d model in obj format
	vertexData = mesh.returnMeshVertices();
	faceData = mesh.returnMeshFaces();
	//vertexData = model.vertices;
/*
	std::cout << "Number of vertices = " << vertexData.size() << std::endl;
	//	std::cout << "Number of vertices = " << model.OBJIndices.size() << std::endl;
	for (int i = 0; i < vertexData.size(); i++)
		std::cout << i << "  " << vertexData[i].x << "  " << vertexData[i].y << "  " << vertexData[i].z << std::endl;

	std::cout << "Number of faces = " << faceData.size() << std::endl;
	for (int i = 0; i < faceData.size(); i++)
		std::cout << i << "  " << faceData[i].x << "  " << faceData[i].y << "  " << faceData[i].z << std::endl;
	system("PAUSE");
*/
	//As exemple we modify one mesh vertex
	//vertexData[5].x = 2.0f;
	//vertexData[5].y = 2.0f;
	//vertexData[5].z = 2.0f;
	// or the same way:	
	//mesh.setVertex(5, 2.0f, 2.0f, 2.0f);

	// Find Aligned Bounding Box (ABB)
	float maxX = -100000.f; //very small value
	float maxY = -100000.f;
	float maxZ = -100000.f;
	float minX = 100000.f;  //very big value
	float minY = 100000.f;
	float minZ = 100000.f;

	for (int i = 0; i < vertexData.size(); i++){
		if (vertexData[i].x > maxX) maxX = vertexData[i].x;
		if (vertexData[i].y > maxY) maxY = vertexData[i].y;
		if (vertexData[i].z > maxZ) maxZ = vertexData[i].z;
		if (vertexData[i].x < minX) minX = vertexData[i].x;
		if (vertexData[i].y < minY) minY = vertexData[i].y;
		if (vertexData[i].z < minZ) minZ = vertexData[i].z;
	}


	maxX++; maxY++; maxZ++;
	minX--; minY--; minZ--;
	int ind;
	std::vector<glm::vec3> arrayBB;
	
	arrayBB.resize((HEIGHT+1)*(WIDTH+1)*(DEPTH+1));
	float sizeCellX = (maxX - minX) / 10.0f;
	float sizeCellY = (maxY - minY) / 10.0f;
	float sizeCellZ = (maxZ - minZ) / 10.0f;

	// Assign values
	for (int i = 0; i <= HEIGHT; ++i) {
		for (int j = 0; j <= WIDTH; ++j) {
			for (int k = 0; k <= DEPTH; ++k) {
				ind = (i*(HEIGHT+1) + j)*(WIDTH+1) + k;
				arrayBB[ind].x = minX + i * sizeCellX;
				arrayBB[ind].y = minY + j*sizeCellY;
				arrayBB[ind].z = minZ + k * sizeCellZ;
			}
		}
	}

	for (int i = 0; i < vertexData.size(); i++) {
		vertexData[i].x = vertexData[i].x - minX;
		vertexData[i].y = vertexData[i].y - minY;
		vertexData[i].z = vertexData[i].z - minZ;
	}



	//HASEMOS UN TAPER CON UNA GRAN PECHONALIDAD WEI
	float taper0 = minX;
	float taper1 = maxX;
	float tapervalue;


	std::vector<glm::vec3> arrayBBtaper = arrayBB;

	// Assign values

	for (int i = 0; i <= HEIGHT; i++) {
		for (int j = 0; j <= WIDTH; j++)		{
			for (int p = 0; p <= DEPTH; p++)			{
				ind = (i*(HEIGHT + 1) + j)*(WIDTH + 1) + p;
				if (arrayBB[ind].x == minX){

				}
				else if (arrayBB[ind].x == maxX)				{
					arrayBBtaper[ind].y = arrayBB[ind].y * 0.5;
					arrayBBtaper[ind].z = arrayBB[ind].z * 0.5;
				}
				else{
					tapervalue = (1 - 0.5f *((arrayBB[ind].x - minX) / (maxX - minX)));
					arrayBBtaper[ind].y = arrayBB[ind].y * tapervalue;
					arrayBBtaper[ind].z = arrayBB[ind].z * tapervalue;
				}
			}
		}
	}

	//C�digo para el twist
	float twistvalue;
	for (int i = 0; i <= HEIGHT; i++) {
		for (int j = 0; j <= WIDTH; j++) {
			for (int p = 0; p <= DEPTH; p++) {
				ind = (i*(HEIGHT + 1) + j)*(WIDTH + 1) + p;
				if (arrayBB[ind].x == minY) {

				}
				else if (arrayBB[ind].x == maxY) {
					twistvalue = glm::radians(75.0f);
					glm::mat3 m1 = { cos(twistvalue), 0, -sin(twistvalue), 0, 1, 0, sin(twistvalue), 0, cos(twistvalue) };
					arrayBBtaper[ind] = arrayBBtaper[ind] * m1;
				}
				else {
					twistvalue = (glm::radians(75.0f) * ((arrayBBtaper[ind].y - minY) / (maxY - minY)));
					glm::mat3 m1 = { cos(twistvalue), 0, -sin(twistvalue), 0, 1, 0, sin(twistvalue), 0, cos(twistvalue) };
					arrayBBtaper[ind] = arrayBBtaper[ind] * m1;
				}
			}
		}
	}


	
	//HASEMOS INTERPOLASION TRILINE� WEI
	
	for (int i = 0; i < vertexData.size(); i++)
	{
		int index1 = floor(vertexData[i].x/sizeCellX);
		int index2 = floor(vertexData[i].y / sizeCellY);
		int index3 = floor(vertexData[i].z / sizeCellZ);

		float xD = ((vertexData[i].x - arrayBBtaper[((index1*(HEIGHT + 1) + index2)*(WIDTH + 1) + index3)].x) / sizeCellX);
		float yD = ((vertexData[i].y - arrayBBtaper[((index1*(HEIGHT + 1) + index2)*(WIDTH + 1) + index3)].y) / sizeCellY);
		float zD = ((vertexData[i].z - arrayBBtaper[((index1*(HEIGHT + 1) + index2)*(WIDTH + 1) + index3)].z) / sizeCellZ);


		glm::vec3 C000 = arrayBBtaper[(index1*(HEIGHT + 1) + index2)*(WIDTH + 1) + index3];
		glm::vec3 C100 = arrayBBtaper[((index1 + 1)*(HEIGHT + 1) + index2)*(WIDTH + 1) + index3];
		glm::vec3 C010 = arrayBBtaper[(index1*(HEIGHT + 1) + (index2 + 1))*(WIDTH + 1) + index3];
		glm::vec3 C110= arrayBBtaper[((index1 + 1)*(HEIGHT + 1) + (index2+1))*(WIDTH + 1) + index3];

		glm::vec3 C001 = arrayBBtaper[(index1*(HEIGHT + 1) + index2)*(WIDTH + 1) + (index3 + 1)];
		glm::vec3 C101 = arrayBBtaper[((index1 + 1)*(HEIGHT + 1) + index2)*(WIDTH + 1) + (index3 + 1)];
		glm::vec3 C011 = arrayBBtaper[(index1*(HEIGHT + 1) + (index2 + 1))*(WIDTH + 1) + (index3 + 1)];
		glm::vec3 C111 = arrayBBtaper[((index1 + 1)*(HEIGHT + 1) + (index2 + 1))*(WIDTH + 1) + (index3 + 1)];


		glm::vec3 C00 = C000 * (1 - xD) + C100 * xD;
		glm::vec3 C10 = C010 * (1 - xD) + C110 * xD;
		glm::vec3 C01 = C001 * (1 - xD) + C101 * xD;
		glm::vec3 C11 = C011 * (1 - xD) + C111 * xD;

		glm::vec3 C0 = C00 * (1 - yD) + C10 * yD;
		glm::vec3 C1 = C01 * (1 - yD) + C11 * yD;

		glm::vec3 C = C0 * (1 - zD) + C1 * zD;

		vertexData[i] = C;
		
	}

	// write the new mesh vertex
	for (int i = 0; i < vertexData.size(); i++) {
		mesh.setVertex(i, vertexData[i].x, vertexData[i].y, vertexData[i].z);
	}
	// create a new file with the modified mesh
	mesh.printObjModel(mesh, fileOut);
	std::cout << "Aplicamos twist y tapper al mismo tiempo y en todo el modelo" << std::endl;
	system("PAUSE");
}